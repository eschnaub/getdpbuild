#!/bin/bash

# get recent compilers
source /cvmfs/sft.cern.ch/lcg/releases/LCG_100/gcc/10.3.0.fp/x86_64-centos7/setup.sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_100/CMake/3.20.0/x86_64-centos7-gcc10-opt/CMake-env.sh

# get openBlas from cvmfs
source /cvmfs/sft.cern.ch/lcg/releases/LCG_100/blas/0.3.10.openblas/x86_64-centos7-gcc10-opt/blas-env.sh

mkdir -p tmp
cd tmp

# compile small static gmsh for getdp for field interpolation (not the real mighty one, that one is installed via python)
git clone https://gitlab.onelab.info/gmsh/gmsh.git
cd gmsh
mkdir lib
cd lib
cmake -DDEFAULT=0 -DENABLE_PARSER=1 -DENABLE_POST=1 -DENABLE_ANN=1 -DENABLE_BLAS_LAPACK=1 -DENABLE_BUILD_LIB=1 -DENABLE_PRIVATE_API=1 -DCMAKE_INSTALL_PREFIX=/eos/project/s/steam/sw ..
make lib -j 7
make install/fast
cd ../..

# build petsc using compiled OpenBLAS
git clone -b release https://gitlab.com/petsc/petsc.git petsc
cd petsc
export PETSC_DIR=$PWD
export PETSC_ARCH=real_mumps_seq

# remove PetSc installation to prevent an error
# done manually since no uninstall script exists afaik
rm -rf /eos/project/s/steam/sw/share/petsc
rm -rf /eos/project/s/steam/sw/lib/petsc
rm -rf /eos/project/s/steam/sw/lib/libpetsc*
rm -rf /eos/project/s/steam/sw/include/petsc
rm -rf /eos/project/s/steam/sw/include/petsc*

./configure CC=$CC CXX=$CXX FC=$FC F77=$FC --prefix=/eos/project/s/steam/sw --with-clanguage=cxx --with-debugging=0 --with-mpi=0 --with-mpiuni-fortran-binding=0 --download-mumps=yes --with-mumps-serial --with-shared-libraries=1 --with-x=0 --with-ssl=0 --with-scalar-type=real --with-blaslapack-lib=/cvmfs/sft.cern.ch/lcg/releases/LCG_100/blas/0.3.10.openblas/x86_64-centos7-gcc10-opt/lib/libopenblas.so

make PETSC_DIR=/eos/project/s/steam/build/getdpbuild/tmp/petsc PETSC_ARCH=real_mumps_seq all -j 7

make install

# install gmsh for python in virtualenvironment (avoiding root pip install for safety reasons)
# also install some other packages needed
cd ../../../../sw

python3 -m venv gmshEnv
source gmshEnv/bin/activate
pip3 install --upgrade pip
pip3 install wheel
pip3 install gmsh
pip3 install numpy
pip3 install pyyaml
pip3 install matplotlib
deactivate

cd ../build/getdpbuild/tmp

# install getdp customized to CERN usage
git clone https://gitlab.cern.ch/steam/cerngetdp.git
cd cerngetdp
mkdir bin
cd bin
cmake  -DENABLE_BLAS_LAPACK=0 -DENABLE_ARPACK=0 -DENABLE_SPARSKIT=0 -DCMAKE_INSTALL_PREFIX=/eos/project/s/steam/sw -DPETSC_LIBS=/eos/project/s/steam/sw/lib/libpetsc.so -DPETSC_INC=/eos/project/s/steam/sw/include -DENABLE_FORTRAN=0 -DCMAKE_INSTALL_RPATH=/eos/project/s/steam/sw/lib ..
make -j 7
make install

cd ../../..
rm -rf tmp/

